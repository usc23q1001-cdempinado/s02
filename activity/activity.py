name = 'Christian Dave O. Empinado'
age = 22
occupation = 'Business Analyst Intern'
movie = 'Interstellar'
rating = 98.5

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%.")

num1, num2, num3 = 12,24,36

print(num1 * num2)
print(num1 < num3)
num2+=num3
print(num2)